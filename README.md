# Wheat Grain Counting

**Note: _TensorFlow is required to use the CNN functionality (CNN.py). If you do
not have TensorFlow already installed, visit [here](https://github.com/tensorflow/tensorflow/blob/master/tensorflow/g3doc/get_started/os_setup.md)
for detailed instructions on how to install it._**
